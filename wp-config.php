<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eclipse_emea_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost:8889');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZSK54_bEDSvQj>*+~n.:)RO{^]-iJ]]?mSAVuE4e,L2wUmx<}zP#aAdG)xd*v}o/');
define('SECURE_AUTH_KEY',  'nWl9;U:m2]$-yOskMxtgy4BrYLoZu{u mYE9_;Fi;($5dO0z0@()+lx{rs^.2sFb');
define('LOGGED_IN_KEY',    ';57oJxSlrLI}.=|4Tuok*dC_z~n@57l-OO2H]@*cFXGU&|1!$ehUL`h7R&S&oWmI');
define('NONCE_KEY',        'nYFCo#KlIdcbfn`OI>`2A(aAAU_Z^8pUifpNEiEmz0F}$tf?m;k,<$}yP0q5$r&[');
define('AUTH_SALT',        '4iM0c>J|ya*JBb~xTyIa$@UP7=)>!I(]5Z+O41+8D1m~W|q-^f%IQ2(CTFe?@hZW');
define('SECURE_AUTH_SALT', 'HB.!*JW:eeUXh0%IV&-cxz.`7hAm[C`K(q9u+J%oOpQ#S~P@.YhEeyP!nw#+A?eH');
define('LOGGED_IN_SALT',   '6:GhOPZAS!N?j3xJ a>uxS($j=[{3ecye-l@V$/$e.yyC.5Gdebv{uEBuOF3u+ME');
define('NONCE_SALT',       'CHO]J{8=I_pHR3BUR5?:`&i)DNB7-&S!gUr>Pe0fB^#9i+lb:4sq<@o=d1dyjtXl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
