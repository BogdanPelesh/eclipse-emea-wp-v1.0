var browserSyncOptions = {
    proxy: "localhost:8888/new.eclipse-emea.com/",
    //port: 8888,
    notify: false
};

var browserSyncWatchFiles = [
    './css/*.min.css',
    './js/*.min.js',
    './*.php'
];

var paths = {
  css:['./css/*.css'],
  scss:['./sass/*.scss'],
  script:['./js/*.js'],
  php: ['./*.php']
};

var gulp            = require('gulp');
var plumber         = require('gulp-plumber');
var sourcemaps      = require('gulp-sourcemaps');
var browserSync     = require('browser-sync');
var reload          = browserSync.reload;
var sass            = require('gulp-sass');
var merge           = require('gulp-merge');
var rename          = require('gulp-rename');
var autoprefixer    = require('gulp-autoprefixer');
var cleanCSS        = require('gulp-clean-css');
var clone           = require('gulp-clone');
var cssnano         = require('gulp-cssnano');
var watch           = require('gulp-watch');
var imagemin        = require('gulp-imagemin');
//var concatCss = require('gulp-concat-css');

gulp.task('scss', function() {
        return gulp.src(['node_modules/bootstrap/scss/*.scss', './sass/*.scss'])
            .pipe(plumber({
                errorHandler: function (err) {
                    console.log(err);
                    this.emit('end');
                }
            }))

            .pipe(sass())
            //.pipe(concatCss("style.css"))
            .pipe(autoprefixer({
                browsers: ['last 4 versions'],
                cascade: false
            }))

            .pipe(cleanCSS({compatibility: '*'}))
            .pipe(cssnano({discardComments: {removeAll: true}}))
            .pipe(rename({suffix: '.min'}))

            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))

            .pipe(gulp.dest('./css'))
});

gulp.task('php', function(){
    gulp.src(paths.php)
        .pipe(reload({stream:true}));
});

gulp.task('watch', function () {
    gulp.watch(paths.scss, ['scss']);
    gulp.watch(paths.php, ['php']);

    gulp.watch(paths.script, ['scripts']);
    gulp.watch('./img/src/**', ['imagemin'])
});

gulp.task('imagemin', function(){
    gulp.src('img/src/**')
        .pipe(imagemin())
        .pipe(gulp.dest('img'))
});

gulp.task('scripts', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(gulp.dest("./js"))
        .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});

gulp.task('default', ['scss', 'scripts','imagemin', 'watch', 'browser-sync'], function () { });